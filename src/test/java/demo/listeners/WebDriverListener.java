package demo.listeners;

import net.thucydides.core.annotations.Managed;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;

/**
 * Listener class used with tests requiring browser.
 * Main role is to init WebDriver and close it after whole suite is complete
 */
public class WebDriverListener {
    @Managed(driver = "chrome")
    WebDriver driver;

    @Before
    public void prepareTest(){
    }


    @After
    public void teardownTest(){
        driver.close();
    }
}
