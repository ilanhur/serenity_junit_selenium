package demo.suites.web;

import demo.listeners.WebDriverListener;
import demo.steps.web.OlxSearchSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Narrative;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(SerenityRunner.class)
@Narrative(text={"Test checking if it`s possible to search via olx"})
public class OlxSearchTest extends WebDriverListener {

    @Steps
    OlxSearchSteps olxSearchSteps;

    @Test
    public void checkSearch() {
        olxSearchSteps.performSearch("Rower");
        olxSearchSteps.verifyResults("Rower", 500);
    }
}
