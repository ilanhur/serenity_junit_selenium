package demo.steps.web;

import demo.pages.OlxSearchPage;
import demo.pages.OlxSearchResultPage;
import demo.steps.core.CoreStep;
import net.thucydides.core.annotations.Step;
import org.hamcrest.core.StringContains;

import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getPages;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assume.*;

public class OlxSearchSteps extends CoreStep {

    OlxSearchPage olxSearchPage;
    OlxSearchResultPage olxSearchResultPage;

    @Step("Perform search for {0} via olx.pl page")
    public void performSearch(String keyword){
        olxSearchPage.displaySearchForm();
        olxSearchPage.typeSearchKeyword(keyword);
        olxSearchResultPage = olxSearchPage.performSearch();
    }

    @Step("Verify that results are displayed and meet expectations - price not higher than {0}")
    public void verifyResults(String keyword, int priceThreshold){
        verifyResultsPageTitle(keyword);
        verifyThatResultsAreNotEmpty();
        verifyResultPrice(priceThreshold);
    }

    @Step("Verify that after search, results page is displayed")
    public void verifyResultsPageTitle(String keyword){
        assumeThat(olxSearchResultPage.getPageTitle(), StringContains.containsString(keyword));
    }

    @Step("Verify that there are any results")
    public void verifyThatResultsAreNotEmpty(){
        assumeFalse(olxSearchResultPage.getResults().isEmpty());
    }

    @Step("Verify price is not higher than {0}")
    public void verifyResultPrice(int priceThreshold){
        OlxSearchResultPage.SearchResult firstElement = olxSearchResultPage.getFirstResult();

        assertThat(firstElement.getPrice() < priceThreshold &&  firstElement.getPrice() > 0);

        addLog("Item found with title:"+ firstElement.getTitle()
                +" and price "+ firstElement.getPrice() + " zł");
    }

    private OlxSearchPage onOlxSearchPage() {
        return getPages().get(OlxSearchPage.class);
    }

    private OlxSearchResultPage onOlxSearchResultPage() {
        return getPages().get(OlxSearchResultPage.class);
    }
}
