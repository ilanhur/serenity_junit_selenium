package demo.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.WebDriver;

@DefaultUrl("http://www.olx.pl")
public class OlxSearchPage extends PageObject {

    @FindBy(id = "headerSearch", timeoutInSeconds="10")
    WebElementFacade inputSearch;

    @FindBy(id = "submit-searchmain")
    WebElementFacade searchButton;

    public OlxSearchPage(WebDriver driver) {
        super(driver);
    }

    public void displaySearchForm(){
        open();
    }

    public void typeSearchKeyword(String keyword) {
        inputSearch.sendKeys(keyword);
    }

    public OlxSearchResultPage performSearch() {
        searchButton.click();
        return this.switchToPage(OlxSearchResultPage.class);
    }
}