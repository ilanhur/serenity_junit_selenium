package demo.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

import static java.time.temporal.ChronoUnit.SECONDS;

public class OlxSearchResultPage extends PageObject {

    //timeout defined along annotation
    @FindBy(css = "#offers_table .offer", timeoutInSeconds="10")
    List<WebElementFacade> searchResults;

    //Ofc we could use searchResult.get(0) - but the point is to demonstrate that FindBy can be used in different ways
    @FindBy(css = "#offers_table .offer")
    WebElementFacade firstSearchResult;

    public OlxSearchResultPage(WebDriver driver) {
        super(driver);
    }

    /**
     * Just to show that we could return a list of objects with fileds that we can define
     * By default we would get a list of WebElements from which we would need to "findElement"
     * to get data from
     * @return
     */
    public List<SearchResult> getResults() {
        List<SearchResult> convertedSearchResults = new ArrayList<>();
        for (WebElement result : searchResults) {
            convertedSearchResults.add(new SearchResult(result));
        }
        return convertedSearchResults;
    }

    public SearchResult getFirstResult() {
        //timeout defined upon first retrieval of elements
        return new SearchResult(firstSearchResult.withTimeoutOf(10, SECONDS));
    }

    public String getPageTitle() {
        return getDriver().getTitle();
    }

    public class SearchResult{

        private String title;
        private double price;

        public SearchResult(WebElement element){
            //findElements are used to verify presence of an element since they do not throw exceptions
            // => return empty list
            List<WebElement> titleElement = element.findElements(By.cssSelector(".title-cell a"));
            List<WebElement> priceElement = element.findElements(By.cssSelector(".price"));
            setTitle((!titleElement.isEmpty() ? titleElement.get(0).getText() : ""));
            setPrice(parsePriceFromString((!priceElement.isEmpty() ? priceElement.get(0).getText() : "0.0")));
        }

        /**
         * Just a simple parser, stripping away everything that is not a number (except for a .)
         * One could try to use NumberFormat, CurrencyInstance with locale and parse
         * but this method is very volatile
         * @param priceString
         * @return
         */
        private double parsePriceFromString(String priceString){
            String cleanPrice = priceString
                    .replaceAll("[^0-9,]", "")
                    .replaceAll(",", ".");
            return Double.valueOf(cleanPrice);
        }

        public void setPrice(double newPrice){
            price = newPrice;
        }

        public double getPrice(){
            return price;
        }

        public void setTitle(String newTitle){
            title = newTitle;
        }

        public String getTitle(){
            return title;
        }

    }
}
